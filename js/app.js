$(document).ready(function() {

  var images = $('.img-thumb li img');
  var show = $('.img-product img');
  var valInput = $(".input-quantity input");
  var cartIcon = $('.prd-relevant .items .cart');
  var cart = [];
  var product = {
    imgSp: "",
    nameSp: "",
    priceSp: "",
    quantitySp: "",
    totalSp: function() {
      return Number(this.priceSp) * Number(this.quantitySp);
    }
  };

  var currentIndex = 0;

  function result(total, value) {
    return Number(total) + Number(value);
  }

  $(cartIcon).click(function() {
    product = {
      imgSp: $(this).closest('.items').find('img').attr('src'),
      nameSp: $(this).closest('.items').find(".inf-prd-relevant .name-relevant").text(),
      priceSp: Number($(this).closest('.items').find(".inf-prd-relevant .price-relevant span").text()),
      quantitySp: 1,
      totalSp: function() {
        return Number(this.priceSp) * Number(this.quantitySp);
      }
    };

    $(".notification").addClass("load");
    setTimeout(function() {
      $(".notification").removeClass("load");
    }, 5000);
    // them display none img noCart
    $(".no-cart").addClass("none");
    // thêm sản phẩm
    addCart();
    // số lượng giỏ hàng
    $(".count-cart span").text(cart.length);
  })

  function showImg() {
    $(this).hover(function() {
      $(images).parent().removeClass("active");
      $(images[currentIndex]).parent().addClass("active");
    })

    show.attr("src", images[currentIndex].src)
  }

  images.each((index, val) => {
    $(val).mouseenter(function() {
      currentIndex = index;
      showImg();
    })
  });

  $("#sub").click(function() {
    if (valInput.val() > 1) {
      z = valInput.val() - 1;
      valInput.val(z);
    }
  });

  $("#add").click(function() {
    z = Number(valInput.val()) + 1;
    valInput.val(z);
  })


  function addCart() {
    //check cart có sản phẩm chưa
    if (cart.length != 0) {
      // kiểm tra array có sản phẩm này chưa

      const checkProduct = obj => cart.some(cartItem => cartItem.nameSp === obj);
      // const getSum = obj => cart.reduce(cartItem => (total, cartItem.totalSp()) => { total + cartItem.totalSp() });

      if (checkProduct(product.nameSp)) {
        const searchName = obj => obj.nameSp === product.nameSp;

        var indexPd = cart.findIndex(searchName);

        // console.log("tong tien:........." + sum);
        cart[indexPd].quantitySp = Number(cart[indexPd].quantitySp) + Number(product.quantitySp);

        $('.detail-cart ul li:nth-child(' + Number(indexPd + 1) + ') .multiplying-price p:last-child').text("x" + cart[indexPd].quantitySp);

        var sum = cart.reduce((total, cartItem) => { return total + cartItem.totalSp(); }, 0);
        $('.total-cart-item p:last-child').text(sum.toLocaleString('vi-VN') + "đ");

      } else {
        //thêm sản phẩm mới
        cart.push(product);

        // let sum = listTotal.reduce(result);
        $(".detail-cart ul").empty();
        for (let i = 0; i < cart.length; i++) {
          var cartItem = `
          <li class = "detail-cart-item flex flex-wrap justify-content-space-between" >
           <div class = "product-cart flex">
          <img src = "` + cart[i].imgSp + `" alt = "" >
                     <p>` + cart[i].nameSp + `</p>
                     </div>
                       <div class = "price-and-quantity" >
                     <div class = "multiplying-price flex" >
                     <p>` + Number(cart[i].priceSp).toLocaleString('vi-VN') + `</p> 
                     <p>x` + cart[i].quantitySp + `</p>
                     </div>
                       <button>Xóa</button>
                     </div> 
                     </div> 
                     </li>`;
          $('.detail-cart ul').append(cartItem);
          var sum = cart.reduce((total, cartItem) => { return total + cartItem.totalSp(); }, 0);
          $('.total-cart-item p:last-child').text(sum.toLocaleString('vi-VN') + "đ");
        }
      }
    } else {
      // cart k có sản phẩm
      cart.push(product);
      var detailCart = `<p class="title">Sản phẩm đã thêm</p> 
          <ul>
             
            
          </ul>
          <div class = "total-cart" >
    <div class = "total-cart-item flex justify-content-space-between" >
    <p> Tổng số tiền: </p>
     <p>` + cart[0].totalSp().toLocaleString('vi-VN') + ` đ</p> 
     </div> 
     <div class = "points-wallet flex justify-content-space-between" >
    <p>Ví tích điểm:</p> 
    <p>+2.000 điểm</p>
     </div> 
     </div> 
     <div class = "check-cart" >
    <button >
    <a href = "#"> Xem giỏ hàng </a> 
    </button> `;
      $(".detail-cart").append(detailCart);
      var cartItem = `<li class = "detail-cart-item flex flex-wrap justify-content-space-between" >
       <div class = "product-cart flex">
          <img src = "` + cart[0].imgSp + `" alt = "" >
                     <p>` + cart[0].nameSp + `</p>
                     </div>
                       <div class = "price-and-quantity" >
                     <div class = "multiplying-price flex" >
                     <p>` + Number(cart[0].priceSp).toLocaleString('vi-VN') + `đ</p> 
                     <p>x` + cart[0].quantitySp + `</p>
                     </div>
                       <button id="del">Xóa</button>
                     </div> 
                     </div>
                     </li> `;
      $('.detail-cart ul').append(cartItem);
    }
  }

  $("#addCart").click(function() {
    product = {
      imgSp: $(".product-briefing .img-thumb li:first-child img").attr("src"),
      nameSp: $(".product-briefing .name-product").text(),
      priceSp: $(".product-briefing .price h3").text(),
      quantitySp: valInput.val(),
      totalSp: function() {
        return Number(this.priceSp) * Number(this.quantitySp);
      }
    };
    // animation cập nhật giỏ hang
    $(".notification").addClass("load");
    setTimeout(function() {
      $(".notification").removeClass("load");
    }, 5000);
    // them display none img noCart
    $(".no-cart").addClass("none");
    // thêm sản phẩm
    addCart();
    // số lượng giỏ hàng
    $(".count-cart span").text(cart.length);
  });
})